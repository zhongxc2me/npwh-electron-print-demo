import Vue from 'vue'
import axios from 'axios'
import ElementUI from 'element-ui'
import inject from './plugins/inject'
import App from './App.vue'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(inject)

axios.defaults.baseURL = 'http://api.npwh.com:88/api'
Vue.prototype.$http = axios

Vue.config.productionTip = false
Vue.use(ElementUI, {
  size: 'small',
  zIndex: 3000
})

Vue.directive('fo', {
  inserted(el, binding, vnode) {
    // 聚焦元素
    el.querySelector('input').focus()
  }
})

new Vue({
  render: h => h(App)
}).$mount('#app')
